# WARNING

Please be advised that this UFO is indeed the version 1.2 UFO, *not* the 
version 1.1 UFO which had a bug in the functional form of the couplings used.  
That bug affected only the magnitude of the predicted cross section and not
the kinematics and was fixed in v1.2.  However, if you look in the _.log_ file
here, you will see this listed as v1.1.

*Be assured that it is actually v1.2*.

For any questions or concerns, please contact [tongylin@gmail.com](mailto:tongylin@gmail.com).